package main

import (
	"flag"
	"gitlab.com/yuanyangen/http_bench/internal"
)

//从命令行中获取参数，并将参数写入benchmark的app中
func main() {
	app := internal.GetBenchApp()
	c := flag.Int("c", 1, "concurrent")
	u := flag.String("u", "", "url")
	f := flag.String("f", "", "filePath")
	d := flag.Int("d", 10, "duration")
	flag.Parse()
	app.SetConcurrent(*c)
	app.SetFilePath(*f)
	app.SetUrl(*u)
	app.SetDuration(*d)
	app.Start()
}
